$(window).scroll(function(evt){
  if($(window).scrollTop()>0){
    $(".navbar").removeClass("navbar_js-top");
  }else{
    $(".navbar").addClass("navbar_js-top");
  }
});
$(function(){
	var map;
	var mapElement = document.getElementById("locat");
	var myCenter = new google.maps.LatLng({lat: 22.597833, lng: 120.304819});
	var mapOptions = {
		center: myCenter,
	    zoom: 17,
	    scrollwheel: true,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var marker = new google.maps.Marker({
	position:myCenter,
	animation: google.maps.Animation.BOUNCE
	});
	var infoMarker = new google.maps.InfoWindow({
	    content: "高雄市前鎮區成功二路四號506"
	});
	infoMarker.open(map,marker);
	map = new google.maps.Map(mapElement, mapOptions);
  	marker.setMap(map);
});
(function($){
	// 全域變數
	var slideIndex = 0;
	$.fn.slides=function(){

		methods.showSlides(slideIndex);
		$(".section-ask__prev").on("click",function(e){
			e.stopPropagation();
			slideIndex--;
			methods.showSlides(slideIndex);
			console.log(methods.showSlides(slideIndex));

		});
		$(".section-ask__next").on("click",function(e){
			e.stopPropagation();
			slideIndex++;
			methods.showSlides(slideIndex);
			console.log(methods.showSlides(slideIndex));

		});


	}
	var methods={
		showSlides : function(n) {
			var i;
			var slides =$(".section-ask__item");

			if (n >= slides.length) {
				// console.log("超過li長度");
				return slideIndex = 0;
			}
			if (n < 0) {
				 // console.log("li長度小於0");
				return slideIndex = slides.length-1;
			}
			for (i = 0; i < slides.length; i++) {
				slides.eq(i).addClass("item_fadeout");
			}
			slides.eq(n).removeClass("item_fadeout");
			return slideIndex;
		}
	};

}(jQuery));